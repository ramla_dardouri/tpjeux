#include <stdio.h>
#include <stdlib.h>
#include <time.h>

char hasard(); // Prototype de la fonction hasard
int comparaison(char joueur, char ordinateur); // Prototype de la fonction comparaison
//
int main() {
    char joueur, ordinateur;
    int resultat;

    // Initialisation du générateur de nombres aléatoires
    srand(time(NULL));

    printf("Jouez à Roche/Papier/Ciseaux (R, P ou C) : ");
    scanf(" %c", &joueur);

    if (joueur != 'R' && joueur != 'P' && joueur != 'C') {
        printf("Choix invalide. Veuillez entrer R, P ou C.\n");
        return 1;
    }

    ordinateur = hasard();
    printf("L'ordinateur choisit : %c\n", ordinateur);

    resultat = comparaison(joueur, ordinateur);

    if (resultat == 0) {
        printf("Égalité !\n");
    } else if (resultat == 1) {
        printf("Vous avez gagné !\n");
    } else {
        printf("L'ordinateur a gagné !\n");
    }

    return 0;
}

